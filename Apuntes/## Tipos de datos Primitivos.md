# CURSO DE YOUTUBE DE C# BÁSICO
## Tipos de datos Primitivos

- sbyte
    > x = 120 (Valores entre **-128 y 127**) - 256 valores diferentes.
- short
    >x = 30000 - Números entre **-32767 y 32767**
-   int 
    >x = 2000000 - Números entre **-2147483648** y **2147483648**
-   float 
    >x = 99.99f - Números entre **1.5x10^-45 y 3.4x10^38** y **2147483648**
-   float 
    >x = 1.5 - Números **mayor que float** (15 de precision, Mundo real)
-   decimal
    >x = 1.5 - Números **mayor que float** (28 de precision, Aplicaciones financieras)

## Ejemplo en código:
#### Integer:
```c#
    int num2 = 13;
    int num3 = 5;
    int sumaTotal = num2 + num3;
```
#### Double:
```c#
    double d2 = 13.2;
    double d3 = 5-4;
    double sumaTotal = d2 + d3;
    Console.WriteLine("La suma total es: " + sumaTotal);
```
#### Integer y string:
> Parseo de strings a integers
```c#
    string d1 = "15"; 
    string d2 = "15"; 
    int d3 = int32.Parse(d1);
    int d4 = int32.Parse(d2);
    
    int sumaTotal = d3 + d4;
    Console.WriteLine("La suma total es: " + sumaTotal);
```
## Uso de constantes:
> Valores que nuncan cambian
```c#
    const double PI = 3.14159265359;
    const int semanas = 7;
    const int meses = 12;
    const int dias = 365;
    Console.WriteLine("Valor: " + semanas);
```

## Uso de métodos
> Sintaxis: 
*_especificador de acceso_* **tipo retorno** *_nombre metodo_* (Lista de parámetros)  { Cuerpo del método   }

#### Ejemplo de un método
```c#
// <especificador de acceso> <tipo retorno> <nombre metodo> (Lista de parámetros)
public static public void MiNombre() {
    Console.WriteLine("Soy liz");
}

// LLamar algo específico
public static public void MiNombre(string nombre) {
    Console.WriteLine(nombre);
} 

// Lamandolo dentro del metodo main
public static void Main(string[] args){
    miNombre();

    miNombre("lizeth");
}
```

#### Ejemplo con varios métodos
```c#
// Sumar
public static public int Sumar(int num1, int num2) {
    return num1 + num2;
}

// Restar
public static public int Restar(int num1, int num2) {
    return num1 - num2;
} 

// Dividir
public static public double Dividir(double num1, double num2) {
    return num1 / num2;
} 

// Lamandolo dentro del metodo main
public static void Main(string[] args){
    Sumar(1, 2);
    Restar(1, 2);
    Dividir(10, 2);
}
```

#### Ejemplo con interacción con el usuario
```c#
// Procesar información
public static public int Calcular() {
    Console.WriteLine("Valor 1");
    string num1 = Console.RedLine();
    Console.WriteLine("Valor 2");
    string num2 = Console.RedLine();

    // Sumar numeros
    int d1 = int.Parse(num1);
    int d2 = int.Parse(num2);

    return d1 + d2;
}


// Lamandolo dentro del metodo main
public static void Main(string[] args){
    Calcular();
}
```

#### Ejemplo con try y catch

```c#
public static void Main(string[] args){
    Console.WriteLine("Valor 1");
    string num1 = Console.RedLine();
    try 
    {
        int d1 = int.Parse(num1);
    } 
    catch (Exception) 
    {
        Console.WriteLine("Formato incorrecto");
    }
    catch (OverflowException) 
    {
        Console.WriteLine("El número es demasiado largo");
    }
    catch (ArgumentNullException) 
    {
        Console.WriteLine("No ingreso un valor...");
    }
    finally 
    {
        Console.WriteLine("Esta frase aparecerá pase lo que pase...");
    }
}
```

## Operadores Unarios
> Define el comportamiento de una variable, hay distintos tipos como: **-**, **+**, etc.
```c#

    //== Unarios ==//

    //Valores enteros
    int numero = 55;
    int numero2 = 54;

    numero = -numero2;

    Console.WriteLine("Valor: {0}", numero);

    // Valores booleanos
    bool bandera = true;
    Console.WriteLine("Valor: {0}", !bandera);

    //== Adición ==//

    //Valores enteros
    int numero3 = 0;
    numero3++;
    Console.WriteLine("Valor: {0}", numero3); //1
    Console.WriteLine("Valor: {0}", numero3++); //1
    Console.WriteLine("Valor: {0}", numero3); //2

    //== Sustracción ==//
    numero3--;
    Console.WriteLine("Valor: {0}", numero3); //1
    Console.WriteLine("Valor: {0}", --numero3); //0
    Console.WriteLine("Valor: {0}", numero3); //0

     //== Matemáticos ==//
     int resultado = numero + numero2;;
     Console.WriteLine("Suma: {0}", resultado); //109
     int resultado = numero - numero2;;
     Console.WriteLine("Resta: {0}", resultado); //1
     int resultado = numero * numero2;;
     Console.WriteLine("Multiplicación: {0}", resultado); //2970
     int resultado = numero / numero2;;
     Console.WriteLine("División: {0}", resultado); //1.018...
     int resultado = numero % numero2;;
     Console.WriteLine("Módulo: {0}", resultado); //1

     //== Relacionales ==
     // menor < ó  mayor >

     //== Igualdad ==//
     // = , !=

     //== Condicionales ==//
     // || ó &&
```
## Estructuras condicionales
> Ayudan a definir que sucede si una expresión cumple o no una condición.
```c#
if {
    // Si cumple
}else{
    // No se cumple
}
// Ejemplo
public static void Main(string[] args){
    string numString = "128";
    float valorParseado;
    bool exito = float.TryParse(numString, out valorParceado);

    if(exito){
        Console.WriteLine("Success::" valorParceado)
    }else {
        Console.WriteLine("Failed parse::")
    }
}
```
## Estructura Switch
> Evalúa distintos casos en los que una condición es válida si cumple.
```c#
switch(valor){
    case 0: 
        //haz algo
        break;
    case 1: 
        //haz algo
        break;
    case 2: 
        //haz algo.
        break;
    default:
        // Evalua cualquier otro caso
}
```
## Estructura if simplificada
> Evalúa todo en una línea de código
```c#
    // condición ? primera_evaluacion : segunda_evaluacion;

    // La expresión a ? b : c ? d : e
    // es evaluada como: a ? b : (c ? d : e)
    // no como (a ? b : c) ? d : e

    //Ejemplo
    int temp = 150;
    estadoAgua = temperatura > 99 ? "gaseoso" : temp < 0 ? "solido" : "liquido";
```
## Ciclos
> Repite una misma instrucción **n** veces.
```c#
    //Ejemplo
    // FOR
    for(valor inicial; condicion ; incremento){
        // Codigo a ejecutar
    }
    // WHILE
    while(condicion){
        // Codigo a ejecutar
        incremento++;
    }
    // DO WHILE
    do{
        // Código a ejecutar
        incremento++;
    }while(condicion)
    // FOREACH
    foreach(){
        // Codigo a ejecutar
    }
```
## Clases
> Construcción de clases
```c#
class Humano {
    //1° Forma
    private string nombre;
    private string apellido;

    public Humano(string nombre, string apellido){
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public Humano(){
    }

    //2° Otra forma de colocar los get y set
    public string Nombre {
        get{ 
            return nombre;
        }
        set{
            nombre = value;
        }
    } 

    // Se puede simplificar
    public string Nombre {
        get => nombre;
        set => nombre = value;
    }

    public void presentar(){
        Console.WriteLine("Soy" + nombre);
    }

    //3° Forma - Ya no necesita el atributo, esta cumple ambos
    public string Nombre {get; set;};
    public string Apellido {get: set;};

    // Ejemplo
    public void presentarNombre(){
        Console.WriteLine("Soy" + Nombre);
    }
}
// Ejemplo
public static void Main(string[] args){
    //1° Forma
    Humano juan = new Humano();
    juan.nombre = "Juan";
    juan.presentar();

    // Con get y set

    //2° Forma de usarlo;
    Humano juan = new Humano();
	juan.Nombre = "Juan";
	Console.WriteLine(juan.Nombre);

    //3° Forma de usarlo;
    Humano juan = new Humano();
	juan.Nombre = "Juan";
	Console.WriteLine(juan.Nombre);
}
```
## Herencia
> - Derived Class (child) - the class that inherits from another class
> - Base Class (parent) - the class being inherited from
> - To inherit from a class, use the : symbol.
```c#
class Vehicle  // base class (parent) 
{
  public string brand = "Ford";  // Vehicle field
  public void honk()             // Vehicle method 
  {                    
    Console.WriteLine("Tuut, tuut!");
  }
}

class Car : Vehicle  // derived class (child)
{
  public string modelName = "Mustang";  // Car field
}

class Program
{
  static void Main(string[] args)
  {
    // Create a myCar object
    Car myCar = new Car();

    // Call the honk() method (From the Vehicle class) on the myCar object
    myCar.honk();

    // Display the value of the brand field (from the Vehicle class) and the value of the modelName from the Car class
    Console.WriteLine(myCar.brand + " " + myCar.modelName);
  }
}
```
## Polimorfismo
>  Inheritance lets us inherit fields and methods from another class. Polymorphism uses those methods to perform different tasks. This allows us to perform a single action in different ways.
```c#
class Animal  // Base class (parent) 
{
  public void animalSound() 
  {
    Console.WriteLine("The animal makes a sound");
  }
}

class Pig : Animal  // Derived class (child) 
{
  public void animalSound() 
  {
    Console.WriteLine("The pig says: wee wee");
  }
}
```
## Abstract
> The abstract keyword is used for classes and methods:

> - Abstract class: is a restricted class that cannot be used to create objects (to access it, it must be inherited from another class).

> - Abstract method: can only be used in an abstract class, and it does not have a body. The body is provided by the derived class (inherited from).
```c#
// Abstract class
abstract class Animal
{
  // Abstract method (does not have a body)
  public abstract void animalSound();
  // Regular method
  public void sleep()
  {
    Console.WriteLine("Zzz");
  }
}

// Derived class (inherit from Animal)
class Pig : Animal
{
  public override void animalSound()
  {
    // The body of animalSound() is provided here
    Console.WriteLine("The pig says: wee wee");
  }
}

class Program
{
  static void Main(string[] args)
  {
    Pig myPig = new Pig(); // Create a Pig object
    myPig.animalSound();  // Call the abstract method
    myPig.sleep();  // Call the regular method
  }
}
```
## Interfaces
> An interface is a completely "abstract class", which can only contain abstract methods and properties (with empty bodies)
```c#
// Interface
interface IAnimal 
{
  void animalSound(); // interface method (does not have a body)
}

// Pig "implements" the IAnimal interface
class Pig : IAnimal 
{
  public void animalSound() 
  {
    // The body of animalSound() is provided here
    Console.WriteLine("The pig says: wee wee");
  }
}

class Program 
{
  static void Main(string[] args) 
  {
    Pig myPig = new Pig();  // Create a Pig object
    myPig.animalSound();
  }
}
```