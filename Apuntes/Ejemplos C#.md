# CURSO DE C# UDEMY
#### Ejemplo sencillo
```c#
// Lamandolo dentro del metodo main
public static void Main(string[] args){
    int year = 2021;
    Console.WriteLine('Varaible' + year);
}
```
#### Ejemplo con string,double,float, decimal e int
```c#
// Lamandolo dentro del metodo main
public static void Main(string[] args){
    int year = Convert.ToInt16("2021");
    double doble = 2.5;
    string nombre = "test";
    decimal decimals = 2.7M;
    float flotante = 8.0F;
    bool valor = true;
    char letra = 'A';
    Decimal de = 28.6M;
    Boolean valor = false;
    Console.WriteLine('Varaible' + year);
}
```
