﻿using JMusic.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JMusic.Data.Contratos
{
    //Interfaz de la logica, escribir waipro - Presenta toda la estructura
    public interface IProductosRepositorio
    {
        Task<List<Producto>> ObtenerProductosAsync();
        Task<(int totalRegistros, IEnumerable<Producto> registros)> ObtenerPaginasProductosAsync(int pagActual, int regPorPagina);
        Task<Producto> ObtenerProductoAsync(int id);
        Task<Producto> Agregar(Producto producto);
        Task<bool> Actualizar(Producto producto);
        Task<bool> Eliminar(int id);
    }

}
