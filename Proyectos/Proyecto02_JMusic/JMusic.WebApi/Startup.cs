using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using JMusic.Data;
using JMusic.Data.Contratos;
using JMusic.Data.Repositorios;
using JMusic.Models;
using JMusic.WebApi.Extensions;
using JMusic.WebApi.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Serilog;

namespace JMusic.WebApi
{
    public class Startup
    {

        //OriginPolicity
        //readonly string MyAllowSpecificOrigins = "CorsPolicy";


        //Se agrego para establecer la configuracion de la base de datos
        // Se agreg� la refencia a Serilog
        public Startup(IConfiguration configuration)
        {
            Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(configuration).CreateLogger();
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(typeof(Startup));

            //Instruccion para aceptar la respuesta en formato XML
            //services.AddControllers().AddXmlDataContractSerializerFormatters();
            services.AddControllers( config => {
                config.ReturnHttpNotAcceptable = true;
            }).AddXmlDataContractSerializerFormatters();

            var connection = Configuration.GetConnectionString("TiendaDb");
            services.AddDbContext<TiendaDbContext>(options =>
            options.UseSqlServer(connection));

            //Creamos las referencias hacia nuestro Repositorio para que sea accesible en todo
            // el proyecto.
            // Scoped - Significa que el servicio sera creado y liberado por cada peticion del cliente
            services.ConfigureDependencies();

            services.AddSingleton<TokenService>();

            //Inicio JWT

            //Se implemeto una extension 
            services.ConfigureJWT(Configuration);

            //Fin JWT

            //Inicio de una politica - Opcion 1

            //Implementacion de una carpeta especifica de Extensiones, para despues
            //Importarlas - Opcion 2
            services.ConfigureCors();
            //Fin de una politica
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddSerilog();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                loggerFactory.AddSerilog();
            }

            app.UseAuthentication();
            app.UseCors();

            
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                    
            });
            

        }
    }
}
