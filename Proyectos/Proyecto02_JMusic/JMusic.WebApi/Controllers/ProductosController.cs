﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using JMusic.Data;
using JMusic.Models;
using JMusic.Data.Contratos;
using AutoMapper;
using JMusic.Dtos;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using JMusic.WebApi.Helpers;

namespace JMusic.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductosController : ControllerBase
    {

        private IProductosRepositorio _productosRepositorio;
        private readonly IMapper _mapper;
        private readonly ILogger<ProductosController> _looger;

        public ProductosController(IProductosRepositorio productosRepositorio, IMapper mapper
            , ILogger<ProductosController> looger)
        {
            _productosRepositorio = productosRepositorio;
            this._mapper = mapper;
            this._looger = looger;
        }

        // GET: api/Productos
        //[HttpGet]
        //[ProducesResponseType(StatusCodes.Status200OK)]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        //public async Task<ActionResult<IEnumerable<ProductoDto>>> GetProductos()
        //{
        //    try
        //    {
        //        var productos = await _productosRepositorio.ObtenerProductosAsync();
        //        return _mapper.Map<List<ProductoDto>>(productos);
        //    }
        //    catch (Exception ex)
        //    {
        //        _looger.LogError($"Error en: {nameof(GetProductos)} ${ex.Message}");
        //        return BadRequest();
        //    }
        //}
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Paginador<ProductoDto>>> GetProductos(int pagActual = 1, int regPorPagina = 3)
        {
            try
            {
                var resultado = await _productosRepositorio.ObtenerPaginasProductosAsync(pagActual, regPorPagina);

                var listaProductosDto = _mapper.Map<List<ProductoDto>>(resultado.registros);

                foreach (ProductoDto p in listaProductosDto) {
                    Console.WriteLine(p.Nombre);
                }

                return new Paginador<ProductoDto>(listaProductosDto, resultado.totalRegistros, pagActual, regPorPagina);
            }
            catch (Exception ex)
            {
                _looger.LogError($"Error en: {nameof(GetProductos)} ${ex.Message}");
                return BadRequest();
            }
        }

        // GET: api/Productos/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<ProductoDto>> GetProducto(int id)
        {
            var producto = await _productosRepositorio.ObtenerProductoAsync(id);

            if (producto == null)
            {
                return NotFound();
            }
            return _mapper.Map<ProductoDto>(producto);
        }

        // POST: api/Productos
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<ProductoDto>> PostProducto(ProductoDto productoDto)
        {
            try
            {
                var producto = _mapper.Map<Producto>(productoDto);
                var nuevoProducto = await _productosRepositorio.Agregar(producto);
                if (nuevoProducto == null)
                {
                    return BadRequest();
                }
                var nuevoProductoDto = _mapper.Map<ProductoDto>(nuevoProducto);
                return CreatedAtAction(nameof(PostProducto), new { id = nuevoProductoDto.Id }, nuevoProductoDto);

            }
            catch (Exception ex)
            {
                _looger.LogError($"Error al: {nameof(PostProducto)} ${ex.Message})");
                return BadRequest();
            }

        }

        // PUT: api/Productos/5
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<ProductoDto>> PutProducto(int id, [FromBody]ProductoDto productoDto)
        {
            if (productoDto == null)
                return NotFound();
            var producto = _mapper.Map<Producto>(productoDto);
            var resultado = await _productosRepositorio.Actualizar(producto);
            if (!resultado)
                return BadRequest();
            return productoDto;
        }

        // DELETE: api/Productos/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteProducto(int id)
        {
            try
            {
                var resultado = await _productosRepositorio.Eliminar(id);
                if (!resultado)
                    return BadRequest();
                return NoContent();
            }
            catch (Exception ex) 
            {
                _looger.LogError($"Error en: {nameof(DeleteProducto)} ${ex.Message})");
                return BadRequest();
            }
        }

    }
}
