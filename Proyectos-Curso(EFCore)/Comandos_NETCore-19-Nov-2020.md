## COMANDOS EN EL API FLUENT DE .NET CORE

1. Add-migration

   - Sirve para crear una migración
   - En el dotnet CLI: **_dotnet ef migrations add_**
   - Parametros:

     - Name
     - OutputDir
     - Context

   - Uso básico. Agregando una migración de nombre "nueva":
     - PMC: **_Add-Migration nueva_**
     - Donet CLI: **_dotnet ef migrations add nueva_**
   - Múltiples contextos de datos:
     - PMC: **_Add-Migration agregar-indices -Context SecondDb Context_**
     - Donet CLI: **_dotnet ef migrations add agregar-indices --context SecondDbContext_**

1. Update-database

   - Este comando tomas las migraciones pendientes y actualiza la base de datos en base a los cambios indicados por las migraciones.
   - El comando quivalente en el dotnet CLI:
     - **_dotnet ef database update_**
   - Parametros:
     - Migration
     - Context
   - Ejemplo 2 . Excluyendo migraciones. Podemos indicar cuales migraciones aplicar. Supongamos que tenemos 3 migraciones: Primera, segunda y tercera. Y queremos aplicar la 1° y 2° migracion:

     - En el PMC: **_Update-database -Migration Segunda_**
     - En el dotnet CLI: **_dotnet ef database update Segunda_**

1. Remove-Migration

   - Este comando sirve para temover migraciones.
   - Si la migracion implica ya se ha aplicado en la base de datos, entonces remover una migracion implica tocar dicha base de datos para revertir los cambios que fueron realizados en ella.
   - En el dotnet CLI: **_dotnet ef migrations remove_**
   - Opciones:
     - Force (Remueve una migracion ya hecha)
   - Ejemplo 1: Uso básico. Removiendo la última migració, la cual no ha sido aplicada en la base de datos.
     - En el PMC: **_Remove-Migration_**
     - En el dotnet CLI: **_dotnet ef migrations remove_**
   - Ejemplo 2: Removiendo la penultima migración.
     - En el PMC:
       - **_Remove-Migration_**
       - **_Remove-Migration_**
     - En el dotnet CLI:
       - **_dotnet ef migrations remove_**
       - **_dotnet ef migrations remove_**
   - Remover una migración que ya fue aplicada en la base de datos. Usamos -force para eso.
     - En el PMC: **_Remove-Migration -Force_**
     - En el dotnet CLI: **_dotnet ef migrations remove --force_**

1. Scaffold-DbContext
   - _Connection_: Este es el connectionstring hacia la base de dato.
   - _Provider_: El proveedor a utilizar. Recordamos que el proveedor es la libreria que hace que EF Core funcione con un motor de base de datos especifico.
   - _OutputDir_: Lugar donde se van a colocar los archivos generados.
   - _Force_: Sobreescribir archivos de ser necesario. Esto significa que cuando se vayan a generar los archivos correspondientes, si un archivo tiene peligro de ser sobreescrito , EF. Core lo que hace es que detiene toda la operacion, sin embargo, si indicamos la opción -force, entonces el archivo va a ser sobreescrito.
   - _Schemas_: Los nombres de los esquemas a los que perteneces las tablas que queremos generar como clases.
   - _Tablas_: Las tablas que queremos generar.
   - _DataAnnotations_ Si queremos que se generen anotaciones de datos cuando sea posible, de lo contrario, todas las configuraciones se mostraran utilizando el API Fluente.
   - Ejemplo 1: Uso básico. Generando todas las tablas:
     - En el PMC: **_Scaffold-DbContext [ConnectionString][proveedor]_**
     - En el dotnet CLI: **_dotnet ef dbcontext scaffold [ConnectionString][proveedor]_**
   - Ejemplo 2: Generando dos tablas especificas. Vamos a generar solamente las tablas Estudiantes y Contactos:
     - En el PMC: **_Scaffold-DbContext [ConnectionString][proveedor] -Tables Estudiantes, Contactos_**
     - En el dotnet CLI: **_dotnet ef dbcontext scaffold [ConnectionString][proveedor] -table Estudiantes --table Contactos_**
1. Drop-database
   - Este comando sirve para borrar una vase de datos.
   - En el dotnet CLI: dotnet ef database drop
   - Opciones:
     - Context: El contexto de datos a utilizar
     - Whatlf: Te informa cúal es la base de datos que resultaría eliminada en el caso deejecutar el comando. Esta opción se puede utilizar en el Package Manager Console.
     - Dry-Run: Te informa cúal es la base de datos que resultaría eliminada en caso de ejecutar el comando. Esta opción se puede utilizar en el dotnet CLI.
   - Drop-databse
   - Ejemplo 1. Uso básico. Borrando una base de datos:
     - En el PMC: **_drop-database -context ApplicationDbContext_**
     - En el dotnet CLI: **_dotnet ef database drop --context ApplicationDbContext_**
   - Ejemplo 2. Verificar la cción previa:
     - En el PMC: **_drop-database -context ApplicationDbContext -whatif_**
     - En el dotnet CLI: **_dotnet ef database drop --context ApplicationDbContext --dry-run_**
1. Get-DbContext
   - Este comando sirve para obtener información respecto al Contexto de Dato.
   - Estas informaciones son: EL nombre del proveedor, el nombre de la base de datos, el nombre del servidor, y si existen determinadas opciones activadas.
   - En el dotnet CLI: **_dotnet ef dbcontext info_**
   - Opciones:
     - Context: El contexto de datos a utilizar
     - Project: El proyecto a utilizar
     - Json: Muestra la información en formato JSON. Esta opción solo está disponible para el dotnet CLI.
   - Ejemplo 1: Uso básico. Obtener la información del Contexto de datos.
     - En el PMC: **_Get-DbContext -Context ApplicationDbContext_**
     - En el dotnet: **_dotnet ef dbcontext info_**
1. Script Migration
   - Con el comando Script-migration podemos generar en SQL los cambios que se harán en una base de datos con una migración.
   - En el dotnet CLI: dotnet ef migrations script.
   - Opciones y parámetros:
     - _From_: La migracion inicial a partir de la cual se va a generar el SQL.
     - _To_: La ultima migración de la cual se va a guardar el código SQL.
     - _Output_: Nombre del archivo donde se va a guardar el código SQL.
     - _Idempotent_: Est opcion realiza verificaciones para ver si se ha aplicado alguna migración, si una migración se ha aplicado antes, entonces no se aplicará nuevamente cuando se ejecute el script.
   - Ejemplo 1: Generando todas las migraciones.
     - En el PMC: **_Script-Migration_**
     - En el dotnet: **_dotnet ef migrations script_**
   - Ejemplo 2: Generando el script en un archivo
     - En el PMC: **_Script-Migration -Output "script.sql"_**
     - En el dotnet: **_dotnet ef migrations script --output "script.sql"_**
   - Ejemplo 3: Generando el script de solo algunas migraciones usando from y to.
     - Supongamos que tenemos las migraciones: Primera, Segunda, Tercera, Cuarta y Quinta; y queremos generar un script con las migraciones desde la Segunda hasta la Cuarta, excluyendo entonces la Primera y la Quinta.
     - En el PMC: **_Script-Migration -from Segunda -to Cuarta_**
     - En el dotnet: **_dotnet ef migrations script Segunda Cuarta_**
   - Ejemplo 4. Generando los scripts de las migraciones que no se han aplicado:
     - En el PMC: **_Script-Migration -Idempotent_**
     - En el PMC: **_dotnet ef migrations script --idempotent_**

---

### PRUEBAS

### TEMAS AVANZADOS

#### División de tablas (Table Splitting)

- Para tener una tabla de cual sea representada por más de un modelo
- Una de las razones por las que hariamos esto sería para simplificar queries de selección
- Utilizar propiedades de navegación entre modelos
- Utilizar el API FLuente para configurar la relación entre ambos modelos y el nombre de la tabla.

#### Entidadesde Propiedad (Owned Entities)

- Las entidades de propiedad son entidades las cuales solamente aparecen en propiedades de navegación de otras entidades.
- A las entidades que tienen entidades de propiedad les llamamos dueñas de la entidad de propiedad.
- Una de las razones principales es poder tener un estandar con respecto a ciertos conjuntos de informaciones.

#### Mapeo de Funciones Escalares

- Las funciones escalares son esas funciones que tu colocas en tu base de datos de SQL Server las cuales pueden recibir parámetros
  y te van a devolver un único valor, titpicamente un string, un booleano, una fecha, etc.

- La idea es que las funciones esten en la base de datos y se van a involucrar desde EF Core.
- Una ventaja de tener funciones escalares es poder controlar de manera precisa el codigo SQL de ciertos queries de tu base de datos.

#### Base de datos Primero

- Nos permite tomar una base dedatos ya existente, y a partir de ella generar todos los modelos y el contexto de datos para poder
  utilizar Entity Framework Core con la BD.
- Podemos utilizar esta técnica con migraciones o sin migraciones.
- Con migraciones podemos tener un historico de futuros cambios.
- Sin migraciones tenemos la opcion de seguir utilizando la BD primero sin dificultades.

1. Puedes realizar cambios "a mano" en tus modelos.
   Ventaja: Lo puedes hacer rapidamente sinb tener que perder cambios.

2. Puedes realizar Scaffold-DbContext nuevamente.
   Ventaja: es automático.

#### Borrado Suave (Soft Delete)

- Significa que, en vez de borrar registros de unabase de datos, vas a marcarlos como borrados.
- Queremos que cada vez que intentemos borrar una entidad que tenga la columna "IsDeleted", se actualice el valor de dicha columna y el registro no se
  borre de la base de datos.
- Una manera de hacer esto es sobreescribiendo el método SaveChanges

#### Conversión de valores

- La conversion de valores nos permite convertir valores.
- El convertidor contiene la lógica para hacer la conversión de un valor a otro y viceversa.
- Centraliza estas conversiones
- EF Core viene con varios convertidores por defecto
