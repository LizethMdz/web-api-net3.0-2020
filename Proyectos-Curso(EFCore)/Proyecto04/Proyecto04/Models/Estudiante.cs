﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proyecto04.Models
{
    public class Estudiante
    {
        public int Id { get; set; }
        public string Nombre { get; set; }

        public DateTime FechaNacimiento { get; set; }
    }
}
