using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Proyecto04.Models;

namespace Proyecto04
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();

            using (var context = new ApplicationDbContext()) {
                // Paso 1 Creamos el objeto
                var estudiate1 = new Estudiante();
                estudiate1.Nombre = "Felipe Galvan";
                estudiate1.FechaNacimiento= new DateTime(1934, 7, 20);

                // Paso 2 Notificamos que queremos agregar un estudiante
                context.Estudiantes.Add(estudiate1);

                // Paso 3. Guardar cambios
                context.SaveChanges();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
