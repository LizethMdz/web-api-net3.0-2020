﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Proyecto05.Migrations
{
    public partial class EstudiantesBecados : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BecaId",
                table: "Estudiantes",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "Estudiantes",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "Beca",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    InstitucionOtorgaBeca = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Porcentaje = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Beca", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Estudiantes_BecaId",
                table: "Estudiantes",
                column: "BecaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Estudiantes_Beca_BecaId",
                table: "Estudiantes",
                column: "BecaId",
                principalTable: "Beca",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Estudiantes_Beca_BecaId",
                table: "Estudiantes");

            migrationBuilder.DropTable(
                name: "Beca");

            migrationBuilder.DropIndex(
                name: "IX_Estudiantes_BecaId",
                table: "Estudiantes");

            migrationBuilder.DropColumn(
                name: "BecaId",
                table: "Estudiantes");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "Estudiantes");
        }
    }
}
