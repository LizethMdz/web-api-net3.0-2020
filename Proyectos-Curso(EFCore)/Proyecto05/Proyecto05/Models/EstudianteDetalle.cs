﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Proyecto05.Models
{
    public class EstudianteDetalle
    {
        public int Id { get; set; }
        public string Cedula { get; set; }
        public int EstudianteId { get; set; }
    }
}
