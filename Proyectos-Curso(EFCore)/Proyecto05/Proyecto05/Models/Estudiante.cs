﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Proyecto05.Models
{
    public class Estudiante
    {
        public int Id { get; set; }

        private string _nombre;

        // Este atributo hace la transformacion del nombre que este mal escrito
        public string Nombre
        {
            get
            {
                return _nombre;
            }
            set
            {
                _nombre = string.Join(' ',
                value.Split(' ').Select(x => x[0].ToString().ToUpper() + x.Substring(1).ToLower()).ToArray());
            }
        }
      
        public DateTime FechaNacimiento { get; set; }
        public bool EstaBorrado { get; set; }
        public List<Contacto> Contactos { get; set; }

        public EstudianteDetalle Detalle { get; set; }

        public List<EstudianteCurso> EstudianteCursos { get; set; }
    }

    public class EstudianteBecado : Estudiante
    {
        public Beca Beca { get; set; }
    }

    public class EstudianteNoBecado : Estudiante
    {

    }

    public class Beca
    {
        public int Id { get; set; }
        public string InstitucionOtorgaBeca { get; set; }
        public decimal Porcentaje { get; set; }
    }
}
