﻿using Proyecto05.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Proyecto05.Codigos
{
    public class Herencia
    {
        public void Codigo()
        {
            using (var context = new ApplicationDbContext())
            {
                // Trae todo tipo de estudiantes, tanto becados como no becados
                var todos = context.Estudiantes.ToList();

                // Trae sólo los estudiantes becados
                var estudiantesBecados = context.Estudiantes.OfType<EstudianteBecado>().ToList();

                // Trae sólo los estudiantes no becados
                var estudiantesNoBecados = context.Estudiantes.OfType<EstudianteNoBecado>().ToList();
            }

            using (var context = new ApplicationDbContext())
            {
                
                //var estudiante = new Estudiante();
                //estudiante.Nombre = "Pablo";
                //estudiante.FechaNacimiento = new DateTime(2000, 1, 25);

                //var beca = new Beca();
                //beca.InstitucionOtorgaBeca = "OCC";
                //beca.Porcentaje = 25;


                //var e = context.EstudiantesBecados.Select(x => x.Beca.Id).FirstOrDefault();
                //Console.WriteLine(e);
                //var estudianteBecado = new EstudianteBecado();
                //estudianteBecado.Nombre = "Maria";
                //estudianteBecado.FechaNacimiento = new DateTime(2005, 2, 15);



                //var estudianteNoBecado = new EstudianteNoBecado();
                //estudianteNoBecado.Nombre = "Oscar";
                //estudianteNoBecado.FechaNacimiento = new DateTime(2001, 3, 12);

                //context.Add(estudiante);
                //context.Add(beca);
                //context.Add(estudianteBecado);
                //context.Add(estudianteNoBecado);

                //context.SaveChanges();

            }

        }
    }
}
