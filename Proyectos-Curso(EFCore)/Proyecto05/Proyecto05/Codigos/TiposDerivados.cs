﻿using Microsoft.EntityFrameworkCore;
using Proyecto05.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Proyecto05.Codigos
{
    public class TiposDerivados
    {
        public void Codigo()
        {
            using (var context = new ApplicationDbContext())
            {

                // Trae todos los estudiantes, y sus becas (si aplica)
                var allStudents = context.Estudiantes
                .Include(x => (x as EstudianteBecado).Beca).ToList();

                // Trae sólo los estudiantes becados con sus becas
                var scholarshipStudents = context.Estudiantes.OfType<EstudianteBecado>()
                .Include(x => x.Beca).ToList();
            }

        }
    }
}
