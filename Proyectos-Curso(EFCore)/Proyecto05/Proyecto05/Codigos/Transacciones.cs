﻿using Proyecto05.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Proyecto05.Codigos
{
    public class Transacciones
    {
        public static void Codigo()
        {
            using (var context = new ApplicationDbContext())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    var estudiante1 = new Estudiante();
                    estudiante1.Nombre = "Robert Fatou";
                    context.Add(estudiante1);
                    context.SaveChanges();
                    // El Id tendrá un valor válido
                    Console.WriteLine(estudiante1.Id);
                    //Si deseamos guardar el registro, hacemos commit
                    transaction.Commit();
                    // Vamos a revertir la operación realizada
                    transaction.Rollback();
                }
            }

        }
    }
}
