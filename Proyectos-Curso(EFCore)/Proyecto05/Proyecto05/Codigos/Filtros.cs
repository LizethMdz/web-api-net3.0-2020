﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Proyecto05.Codigos
{
    public class Filtros
    {
        public void Codigo()
        {
            using (var context = new ApplicationDbContext())
            {
                var students = context.Estudiantes
                .Where(x => x.FechaNacimiento >= DateTime.Today.AddYears(-30)).ToList();

                //Multiples filtros
                var students2 = context.Estudiantes
               .Where(x => x.FechaNacimiento >= DateTime.Today.AddYears(-30) || x.Id == 3).ToList();
            }

            using (var context = new ApplicationDbContext())
            {
                var student = context.Estudiantes.FirstOrDefault(x => x.Id == 5);
            }
        }
    }
}
