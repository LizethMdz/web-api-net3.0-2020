﻿using Proyecto05.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Proyecto05.Codigos
{
    class CreandoRegistros
    {
        public void Codigo()
        {
            using (var context = new ApplicationDbContext())
            {
                // paso 1: Creamos el objeto
                var student1 = new Estudiante();
                student1.Nombre = "Pedro Stewart";
                student1.FechaNacimiento = new DateTime(1993, 8, 25);

                var student2 = new Estudiante();
                student2.Nombre = "Pedro Muck";
                student2.FechaNacimiento = new DateTime(1993, 8, 25);

                // Nota para crear otro objeto podemos instanciarlo con las mismas 
                // caracteristicas.

                // O bien crear una lista de Estudiantes
                var estudiantes = new List<Estudiante>() { student1, student2 };

                // paso 2: Notificamos que queremos agregar un estudiante
                context.Estudiantes.Add(student1);
                // Para guardar una lista de estudiantes
                context.AddRange(estudiantes);

                // paso 3: Guardamos los cambios
                context.SaveChanges();
            }

        }
    }
}
