﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Proyecto05.Codigos
{
    public class LeyendoRegistrosIndividuales
    {
        public void Codigo()
        {
            using (var context = new ApplicationDbContext())
            {
                var estudiante = context.Estudiantes.FirstOrDefault();

                var estudiante2 = context.Estudiantes
                    .First(x => x.Nombre.StartsWith("P"));

                var estudiante3 = context.Estudiantes
                   .Select(x => x.Nombre)
                   .FirstOrDefault();


                if (estudiante != null)
                {
                    // Procesar el estudiante
                }
            }

        }
    }
}
