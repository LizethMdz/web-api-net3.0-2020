﻿using Microsoft.EntityFrameworkCore;
using Proyecto05.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Proyecto05
{
    public class ApplicationDbContext: DbContext
    {
        

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=(localdb)\\ProjectsV13;Initial Catalog=DemoDb;Integrated Security=True")
                // descomenta la siguiente línea para utilizar Carga Perezosa
                //.UseLazyLoadingProxies()
                .EnableSensitiveDataLogging(true);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Mapeo flexible
            //modelBuilder.Entity<Estudiante>().Property(x => x.Nombre).HasField("_nombre");

            // Filtro a nivel del modelo
            //modelBuilder.Entity<Estudiante>().HasQueryFilter(x => x.EstaBorrado == false);

            // Data Seeding
            //var estudiante1 = new Estudiante() { Id = 7, Nombre = "Robert Seed", FechaNacimiento = new DateTime(1990, 4, 12), EstaBorrado = false };
            //modelBuilder.Entity<Estudiante>().HasData(estudiante1);

            modelBuilder.Entity<EstudianteCurso>().HasKey(x => new { x.CursoId, x.EstudianteId });

            /*modelBuilder.Entity<Estudiante>()
                .HasDiscriminator<int>("TipoEstudiante")
                .HasValue<Estudiante>(1)
                .HasValue<EstudianteBecado>(2)
                .HasValue<EstudianteNoBecado>(3);*/

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Estudiante> Estudiantes { get; set; }
        public DbSet<EstudianteBecado> EstudiantesBecados { get; set; }
        public DbSet<EstudianteNoBecado> EstudiantesNoBecados { get; set; }
        public DbSet<EstudianteDetalle> EstudianteDetalles { get; set; }
        public DbSet<EstudianteCurso> EstudiantesCursos { get; set; }
        public DbSet<Curso> Cursos { get; set; }
        public DbSet<Contacto> Contactos { get; set; }
    }
}
